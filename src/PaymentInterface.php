<?php namespace Framework\Shop;

interface PaymentInterface
{
	/**
	 * @param array|Product[] $products
	 * @param string|null     $invoice_number
	 *
	 * @return $this
	 */
	public function setOrder(array $products, string $invoice_number = null);

	public function getLink() : string;
}
