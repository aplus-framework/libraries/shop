<?php namespace Framework\Shop;

class Product
{
	public string $id;
	public string $title;
	public float $price;
	public int $quantity = 1;
	public array $attributes = [];

	public function getSubtotal() : float
	{
		return $this->price * $this->quantity;
	}
}
