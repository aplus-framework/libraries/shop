<?php namespace Framework\Shop;

class Cart
{
	public function __construct()
	{
		if (\session_status() !== \PHP_SESSION_ACTIVE) {
			//throw new \LogicException('Cart requires session activated');
		}
		if (empty($_SESSION['__cart'])) {
			$_SESSION['__cart'] = [];
		}
	}

	/**
	 * @param Product $product
	 *
	 * @throws \JsonException
	 *
	 * @return string Row id
	 */
	public function add(Product $product) : string
	{
		$row_id = \md5(\json_encode([
			'id' => $product->id,
			'price' => $product->price,
			'attributes' => $product->attributes,
		], \JSON_THROW_ON_ERROR));

		if (isset($_SESSION['__cart'][$row_id])) {
			$_SESSION['__cart'][$row_id]->quantity += $product->quantity;
		} else {
			$_SESSION['__cart'][$row_id] = $product;
		}

		return $row_id;
	}

	/**
	 * Update one row.
	 *
	 * @param string $row_id
	 * @param int    $quantity
	 *
	 * @return $this
	 */
	public function update(string $row_id, int $quantity)
	{
		if ($quantity <= 0) {
			unset($_SESSION['__cart'][$row_id]);
			return $this;
		}
		if (isset($_SESSION['__cart'][$row_id])) {
			$_SESSION['__cart'][$row_id]->quantity = $quantity;
		}
		return $this;
	}

	/**
	 * Get a product item.
	 *
	 * @param string $row_id
	 *
	 * @return Product|null
	 */
	public function get(string $row_id) : ?Product
	{
		return $_SESSION['__cart'][$row_id] ?? null;
	}

	/**
	 * Get all product items.
	 *
	 * @return array|Product[]
	 */
	public function getItems() : array
	{
		return $_SESSION['__cart'];
	}

	/**
	 * Remove a item by row id.
	 *
	 * @param string $row_id Row id
	 *
	 * @return $this
	 */
	public function remove(string $row_id)
	{
		unset($_SESSION['__cart'][$row_id]);
		return $this;
	}

	/**
	 * Count total quantity of items in the cart.
	 *
	 * @return int
	 */
	public function count() : int
	{
		$count = 0;
		foreach ($_SESSION['__cart'] as $item) {
			$count += $item->quantity;
		}
		return $count;
	}

	/**
	 * Get total price of the cart.
	 *
	 * @return float
	 */
	public function total() : float
	{
		$total = 0.0;
		/**
		 * @var Product $item
		 */
		foreach ($_SESSION['__cart'] as $item) {
			$total += $item->getSubtotal();
		}
		return $total;
	}

	/**
	 * Get total price of a row id.
	 *
	 * @param string $row_id
	 *
	 * @return float
	 */
	public function itemTotal(string $row_id) : float
	{
		return $_SESSION['__cart'][$row_id]->getSubtotal();
	}

	/**
	 * Clear the cart.
	 *
	 * @return $this
	 */
	public function clear()
	{
		$_SESSION['__cart'] = [];
		return $this;
	}
}
