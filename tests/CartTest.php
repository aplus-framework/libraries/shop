<?php namespace Tests\Shop;

use Framework\Shop\Cart;
use Framework\Shop\Product;
use PHPUnit\Framework\TestCase;

class CartTest extends TestCase
{
	protected Cart $cart;

	public function setup() : void
	{
		$_SESSION = [];
		$this->cart = new Cart();
	}

	protected function getProduct(string $id) : Product
	{
		$product = new Product();
		$product->id = $id;
		$product->title = 'Monitor LG';
		$product->price = 400.00;
		return $product;
	}

	public function testAdd()
	{
		$product = new Product();
		$product->id = 'ABC123';
		$product->price = 9.90;
		$product->title = 'Pen Drive 2GB';

		$this->assertIsString($this->cart->add($product));
		$this->assertCount(1, $this->cart->getItems());
	}

	public function testItemTotal()
	{
		$product = new Product();
		$product->id = 'FOO123';
		$product->title = 'Teclado Positivo';
		$product->price = 10;
		$row_id = $this->cart->add($product);
		$this->assertEquals(10, $this->cart->itemTotal($row_id));

		$product = new Product();
		$product->id = 'BAR456';
		$product->title = 'Monitor LG';
		$product->price = 400;
		$product->quantity = 2;
		$row_id = $this->cart->add($product);
		$this->assertEquals(800, $this->cart->itemTotal($row_id));

		$this->assertEquals(810, $this->cart->total());
	}

	public function testTotal()
	{
		$this->cart->add($this->getProduct('ABC123'));
		$this->assertEquals(400, $this->cart->total());
		$product = $this->getProduct('ABC456');
		$product->quantity = 5;
		$this->cart->add($product);
		$this->assertEquals(2400, $this->cart->total());
	}

	public function testCount()
	{
		$product = $this->getProduct('ABC');
		$this->cart->add($product);
		$this->cart->add($product);
		$product = $this->getProduct('DEF');
		$product->quantity = 2;
		$this->cart->add($product);
		$product = $this->getProduct('GHI');
		$product->quantity = 4;
		$this->cart->add($product);
		$this->assertEquals(8, $this->cart->count());
	}

	public function testGetAndRemove()
	{
		$product = new Product();
		$product->id = '123';
		$product->price = 10;

		$row_id = $this->cart->add($product);
		$this->assertEquals($product, $this->cart->get($row_id));
		$this->cart->remove($row_id);
		$this->assertNull($this->cart->get($row_id));
	}

	public function testClear()
	{
		$product = new Product();
		$product->id = '123';
		$product->price = 10;

		$this->cart->add($product);
		$this->assertEquals(1, $this->cart->count());
		$this->cart->clear();
		$this->assertEquals(0, $this->cart->count());
	}
}
