# Framework Shop Package

- [Homepage](https://the-framework.gitlab.io/libraries/shop.html)
- [API Documentation](https://the-framework.gitlab.io/libraries/shop/docs/)

[![Build](https://gitlab.com/the-framework/libraries/shop/badges/master/pipeline.svg)](https://gitlab.com/the-framework/libraries/shop/-/jobs)
[![Coverage](https://gitlab.com/the-framework/libraries/shop/badges/master/coverage.svg?job=test:php)](https://the-framework.gitlab.io/libraries/shop/coverage/)
